package com.example.snapshot_system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@MapperScan("com.example.snapshot_system.mapper")
public class SnapshotSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnapshotSystemApplication.class, args);
    }

}
