package com.example.snapshot_system.service;

import com.example.snapshot_system.vo.FriendOwnVo;

import java.util.List;

public interface FriendOwnVoService extends ISuperService<FriendOwnVo> {
    List<FriendOwnVo> getFriendOwnVo(String phone);
}
