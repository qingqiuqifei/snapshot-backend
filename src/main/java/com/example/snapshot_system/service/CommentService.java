package com.example.snapshot_system.service;

import com.example.snapshot_system.entity.Comment;

import java.util.List;

/**
 * <p>
 * 会话信息表 服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface CommentService extends ISuperService<Comment> {
    boolean add(String phone_send, String phone_received, String content ,int isText);

    boolean deleteById(int id);

    List<Comment> getComments(String phone);

    boolean getIsCommentsRead(String phone);

    List<Comment> getCommentsFriend(String phone_send, String phone_received);
}
