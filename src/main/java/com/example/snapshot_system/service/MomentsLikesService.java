package com.example.snapshot_system.service;

import com.example.snapshot_system.entity.Moments;
import com.example.snapshot_system.entity.MomentsLikes;

import java.util.List;

/**
 * <p>
 * 朋友圈动态点赞 服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */

public interface MomentsLikesService extends ISuperService<MomentsLikes>{

    boolean add(int id,String phone);

    boolean getIsMomentsLike(int id,String phone);
}
