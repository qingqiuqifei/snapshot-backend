package com.example.snapshot_system.service;

import com.example.snapshot_system.entity.NoticeComments;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.snapshot_system.entity.SysUser;

import java.util.*;

/**
 * <p>
 * 新闻的回复 服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface NoticeCommentsService extends ISuperService<NoticeComments> {

    /**
     * 添加新闻公告评论
     * @param noticeComments
     * @return
     */
    boolean add(NoticeComments noticeComments);

    boolean deleteById(int id);

    List<NoticeComments> getById(int id);
}
