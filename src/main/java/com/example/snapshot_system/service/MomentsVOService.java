package com.example.snapshot_system.service;

import com.example.snapshot_system.mapper.MomentsVOMapper;
import com.example.snapshot_system.vo.MomentsVO;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MomentsVOService extends ISuperService<MomentsVO> {
    List<MomentsVO> getMomentsVo(String phone);
}
