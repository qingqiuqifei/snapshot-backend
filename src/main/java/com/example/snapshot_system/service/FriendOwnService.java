package com.example.snapshot_system.service;

import com.example.snapshot_system.dto.SearchDTO;
import com.example.snapshot_system.entity.FriendOwn;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.snapshot_system.entity.Notice;
import com.example.snapshot_system.vo.FriendOwnVo;
import com.example.snapshot_system.vo.PageVO;

import java.util.List;

/**
 * <p>
 * 好友列表 服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface FriendOwnService extends ISuperService<FriendOwn> {
    /**
     *
     * @param phone
     * @return FriendOwn
     */
    List<FriendOwn> getFriendOwn(String phone);

    boolean addFriend(String phone,String friendPhone);

    boolean deleteFriend(String phone,String friendPhone);
}
