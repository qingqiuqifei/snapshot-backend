package com.example.snapshot_system.service;

import com.example.snapshot_system.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.snapshot_system.vo.PageVO;
import com.example.snapshot_system.dto.SearchDTO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-10
 */
public interface SysUserService extends ISuperService<SysUser> {
    /**
     * 用户分页查询
     * @param searchDto
     * @return
     */
    PageVO<SysUser> getPage(SearchDTO searchDto);

    void add(SysUser sysUser);

    boolean delete(String id);

    boolean updateUser(String phone,String name,String gender,String profess,String introduction);

    int login(String phone,String password);

    int register(String phone,String password,String confirm);

    SysUser getByPhone(String phone);

    boolean updateAvatar(String phone,String avatar);

}
