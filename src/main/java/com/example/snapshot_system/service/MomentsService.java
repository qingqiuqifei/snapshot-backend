package com.example.snapshot_system.service;

import com.example.snapshot_system.entity.Moments;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.snapshot_system.entity.Notice;

import java.util.List;

/**
 * <p>
 * 朋友圈动态 服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface MomentsService extends ISuperService<Moments> {
    List<Moments> getMoments(String phone_send);

    boolean add(String phone_send, String name, String content, String pic , String avatar);

    boolean deleteById(int id);

    boolean addLikes(int id);

}
