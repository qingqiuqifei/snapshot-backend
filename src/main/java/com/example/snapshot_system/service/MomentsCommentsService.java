package com.example.snapshot_system.service;

import com.example.snapshot_system.entity.MomentsComments;

import java.util.List;

/**
 * <p>
 * 朋友圈的评论 服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface MomentsCommentsService extends ISuperService<MomentsComments> {
    /**
     * 添加动态评论
     * @return
     */
    boolean add(int id,String phone_called, String name,String content);

    boolean deleteById(int unique_id);

    List<MomentsComments> getById(int id);
}
