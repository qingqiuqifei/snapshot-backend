package com.example.snapshot_system.service;

import com.example.snapshot_system.entity.FriendApply;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.snapshot_system.entity.FriendOwn;

import java.util.List;

/**
 * <p>
 * 申请好友的列表 服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface FriendApplyService extends ISuperService<FriendApply> {

    /**
     *
     * @param phone
     * @return
     */
    List<FriendApply> getFriendOwn(String phone);

    boolean addFriendApply(String phone,String friendPhone);

    boolean deleteFriendApply(String phone,String friendPhone);

}
