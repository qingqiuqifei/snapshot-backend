package com.example.snapshot_system.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.snapshot_system.dto.SearchDTO;
import com.example.snapshot_system.vo.PageVO;
import com.example.snapshot_system.dto.SearchDTO;
import com.example.snapshot_system.vo.PageVO;

/**
 * @ClassName : ISuperService
 * @author :jin
 * @Date : "2021-7-10"
 */

public interface ISuperService<T> extends IService<T> {
    default PageVO<T> page(SearchDTO searchDTO, LambdaQueryWrapper<T> queryWrapper) {
        Page<T> searchPage = this.packSearchPage(searchDTO);
        Page<T> page = (Page) this.page((IPage) searchPage, (Wrapper) queryWrapper);
        return new PageVO(page);
    }

    default Page<T> packSearchPage(SearchDTO searchDTO) {
        Page<T> searchPage = new Page();
        if (searchDTO.getCurrent() != null && searchDTO.getCurrent() > 0) {
            searchPage.setCurrent((long) searchDTO.getCurrent());
        } else {
            searchPage.setCurrent(1L);
        }

        Integer current = searchDTO.getCurrent();
        if (current == null) {
            current = 1;
        }

        Integer size = searchDTO.getSize();
        if (size == null || size <= 0) {
            size = 10;
        }

        if (size > 500) {
            size = 500;
        }

        searchPage.setSize((long) size);
        searchPage.setCurrent((long) current);
        return searchPage;
    }
}
