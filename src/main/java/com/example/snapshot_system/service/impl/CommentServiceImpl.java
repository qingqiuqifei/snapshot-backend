package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;
import com.example.snapshot_system.entity.Comment;
import com.example.snapshot_system.entity.SysUser;
import com.example.snapshot_system.mapper.CommentMapper;
import com.example.snapshot_system.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 会话信息表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {
    @Autowired
    CommentMapper commentMapper;

    @Override
    public boolean add(String phone_send, String phone_received, String content ,int isText) {
        //将最新消息标志为1的置成0
        UpdateWrapper<Comment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("phone_send",phone_send).eq("phone_received",phone_received).eq("isNewest",true).set("isNewest",false);
        this.update(null, updateWrapper);
        //将最新消息反向标志为1的置成0
        UpdateWrapper<Comment> updateWrapper1 = new UpdateWrapper<>();
        updateWrapper1.eq("phone_send",phone_received).eq("phone_received",phone_send).eq("isNewest",true).set("isNewest",false);
        this.update(null, updateWrapper1);

        //保存通话记录
        Comment comment = new Comment();
        comment.setPhone_send(phone_send);
        comment.setPhone_received(phone_received);
        comment.setContent(content);
        comment.setIsText(isText);
        comment.setCreate_time(new Date());
        comment.setNewest(true);
        this.save(comment);

        //保存通话记录，发送者和接收者互换
        Comment comment1 = new Comment();
        comment1.setPhone_send(phone_received);
        comment1.setPhone_received(phone_send);
        comment1.setContent(content);
        comment1.setIsText(isText);
        comment1.setCreate_time(new Date());
        comment1.setNewest(true);
        this.save(comment1);
        return true;
    }

    @Override
    public boolean deleteById(int id) {
        LambdaQueryWrapper<Comment> qw = new LambdaQueryWrapper<>();
        qw.eq(Comment::getId,id);
        int rows = commentMapper.delete(qw);

        return rows == 0?false:true;
    }

    @Override
    public List<Comment> getComments(String phone) {
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Comment::getPhone_send, phone);
        List<Comment> commentsList = list(queryWrapper);
        //按时间排序
        Collections.sort(commentsList, (o1, o2) -> o2.getCreate_time().compareTo(o1.getCreate_time()));

        return commentsList;
    }

    @Override
    public boolean getIsCommentsRead(String phone) {
        //如果List长度不为0说明有未读信息,返回true
        //判断有未读信息就要返回未读信息的长度，提示“共有x条信息未读”
        TableInfoHelper.initTableInfo(new MapperBuilderAssistant(new MybatisConfiguration(), ""), Comment.class);
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
//        queryWrapper.lambda().eq(Comment::getPhone_send, phone).eq(Comment::isRead,false);
        queryWrapper.like("phone_send",phone);
        queryWrapper.like("isRead",0);
        List<Comment> commentsList = list(queryWrapper);

        return commentsList.size()!=0 ? true:false;
    }

    @Override
    public List<Comment> getCommentsFriend(String phone_send, String phone_received) {
        //将未读信息设置成已读信息
        UpdateWrapper<Comment> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("phone_send",phone_received).eq("phone_received",phone_send).eq("isRead",false).set("isRead",true);
        this.update(null, updateWrapper);

        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Comment::getPhone_send, phone_send).eq(Comment::getPhone_received,phone_received);
        List<Comment> commentsList = list(queryWrapper);


        //按时间排序
        Collections.sort(commentsList, (o1, o2) -> o2.getCreate_time().compareTo(o1.getCreate_time()));

        return commentsList;
    }
}
