package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.entity.FriendOwn;
import com.example.snapshot_system.entity.SysUser;
import com.example.snapshot_system.mapper.FriendOwnMapper;
import com.example.snapshot_system.mapper.FriendOwnVoMapper;
import com.example.snapshot_system.service.FriendOwnService;
import com.example.snapshot_system.service.FriendOwnVoService;
import com.example.snapshot_system.service.SysUserService;
import com.example.snapshot_system.vo.FriendOwnVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FriendOwnVoServiceImpl extends ServiceImpl<FriendOwnVoMapper, FriendOwnVo> implements FriendOwnVoService {
    @Autowired
    private FriendOwnService friendOwnService;

    @Autowired
    private SysUserService sysUserService;

    @Override
    public List<FriendOwnVo> getFriendOwnVo(String phone) {
        //返回用户的号码，头像，姓名
        List<FriendOwn> friendOwns = friendOwnService.getFriendOwn(phone);
        List<FriendOwnVo> friendOwns1 = new ArrayList<>();
        for(FriendOwn friendown:friendOwns){
            FriendOwnVo friendOwnVo = new FriendOwnVo();
            friendOwnVo.setMyself_phone(friendown.getMyself_phone());
            friendOwnVo.setFriend_phone(friendown.getFriend_phone());
            friendOwnVo.setName(sysUserService.getByPhone(friendown.getFriend_phone()).getName());
            friendOwnVo.setAvatar(sysUserService.getByPhone(friendown.getFriend_phone()).getAvatar());
            friendOwns1.add(friendOwnVo);
        }

        return friendOwns1;
    }
}
