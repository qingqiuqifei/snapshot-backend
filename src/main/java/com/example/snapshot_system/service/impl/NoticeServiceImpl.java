package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.snapshot_system.dto.SearchDTO;
import com.example.snapshot_system.entity.Notice;
import com.example.snapshot_system.mapper.NoticeMapper;
import com.example.snapshot_system.service.NoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.vo.PageVO;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 公告 服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-16
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    @Override
    public PageVO<Notice> getPage(SearchDTO searchDto) {
        LambdaQueryWrapper<Notice> qw = new LambdaQueryWrapper<>();
        qw.like(Notice::getTitle, searchDto.getSearchKey());
        PageVO<Notice> page = page(searchDto,qw);
        return page;
    }

    @Override
    public boolean add(Notice notice) {
        notice.setGmtCreate(new Date());
        notice.setGmtUpdate(new Date());
        this.save(notice);
        return true;
    }

    @Override
    public boolean deleteByTitle(String title) {
        UpdateWrapper<Notice> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("title",title).set("deleted", 1);
        boolean isDeleted = this.update(null, updateWrapper);
        return isDeleted;
    }

    @Override
    public boolean updateNotice(Notice notice) {
        UpdateWrapper<Notice> updateWrapper = new UpdateWrapper<>();
        Long id = notice.getId();
        updateWrapper.eq("id",id).set("title",notice.getTitle());
        updateWrapper.eq("id",id).set("content",notice.getContent());
        updateWrapper.eq("id",id).set("gmt_update",new Date());
        updateWrapper.eq("id",id).set("name",notice.getName());
        updateWrapper.eq("id",id).set("avatar",notice.getAvatar());
        updateWrapper.eq("id",id).set("likes",notice.getLikes());

        boolean isUpdated = this.update(null, updateWrapper);
        return isUpdated;
    }
}
