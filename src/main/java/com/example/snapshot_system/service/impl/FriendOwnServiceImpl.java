package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.snapshot_system.entity.FriendOwn;
import com.example.snapshot_system.mapper.FriendOwnMapper;
import com.example.snapshot_system.mapper.FriendOwnVoMapper;
import com.example.snapshot_system.service.FriendOwnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.vo.FriendOwnVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 好友列表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Service
public class FriendOwnServiceImpl extends ServiceImpl<FriendOwnMapper, FriendOwn> implements FriendOwnService {
    @Autowired
    FriendOwnMapper friendOwnMapper;

    @Autowired
    FriendOwnVoMapper friendOwnVoMapper;

    @Override
    public List<FriendOwn> getFriendOwn(String phone) {
        QueryWrapper<FriendOwn> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(FriendOwn::getMyself_phone, phone);
        List<FriendOwn> friendOwnList = list(queryWrapper);

        //返回用户的号码，头像，姓名
        return friendOwnList;
    }

    @Override
    public boolean addFriend(String myself_phone, String friend_phone)
    {
        LambdaQueryWrapper<FriendOwn> qw = new LambdaQueryWrapper<>();
        qw.eq(FriendOwn::getMyself_phone,myself_phone).eq(FriendOwn::getFriend_phone,friend_phone);
        List<FriendOwn> friendOwnList = list(qw);

        //已经有好友了
        if(friendOwnList.size() !=0){
            return false;
        }

        FriendOwn fr = new FriendOwn();
        fr.setMyself_phone(myself_phone);
        fr.setFriend_phone(friend_phone);
        this.save(fr);
        return true;
    }

    @Override
    public boolean deleteFriend(String phone, String friendPhone) {
        LambdaQueryWrapper<FriendOwn> qw = new LambdaQueryWrapper<>();
        qw.eq(FriendOwn::getMyself_phone,phone).eq(FriendOwn::getFriend_phone,friendPhone);
        int rows = friendOwnMapper.delete(qw);

        return rows == 0?false:true;
    }
}
