package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.snapshot_system.entity.FriendOwn;
import com.example.snapshot_system.entity.NoticeComments;
import com.example.snapshot_system.mapper.NoticeCommentsMapper;
import com.example.snapshot_system.service.NoticeCommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 新闻的回复 服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Service
public class NoticeCommentsServiceImpl extends ServiceImpl<NoticeCommentsMapper, NoticeComments> implements NoticeCommentsService {
    @Autowired
    NoticeCommentsMapper noticeCommentsMapper;


    @Override
    public boolean add(NoticeComments noticeComments) {
        LambdaQueryWrapper<NoticeComments> qw = new LambdaQueryWrapper<>();

        //信息不全
        if(     noticeComments.getId().equals(null) ||
                noticeComments.getPhone_called().equals(null) ||
                noticeComments.getContent().equals(null)){
            return false;
        }

        NoticeComments fr = new NoticeComments();
        fr.setId(noticeComments.getId());
        fr.setPhone_called(noticeComments.getPhone_called());
        fr.setContent(noticeComments.getContent());
        this.save(fr);
        return true;
    }

    @Override
    public boolean deleteById(int unique_id) {
        LambdaQueryWrapper<NoticeComments> qw = new LambdaQueryWrapper<>();
        qw.eq(NoticeComments::getUnique_id,unique_id);
        int rows = noticeCommentsMapper.delete(qw);

        return rows == 0?false:true;
    }

    @Override
    public List<NoticeComments> getById(int id) {
        QueryWrapper<NoticeComments> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(NoticeComments::getId, id);
        List<NoticeComments> NoticeCommentsList = list(queryWrapper);
        return NoticeCommentsList;
    }
}
