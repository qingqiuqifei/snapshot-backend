package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.snapshot_system.entity.Comment;
import com.example.snapshot_system.entity.Moments;
import com.example.snapshot_system.mapper.CommentMapper;
import com.example.snapshot_system.mapper.MomentsMapper;
import com.example.snapshot_system.mapper.SysUserMapper;
import com.example.snapshot_system.dto.SearchDTO;
import com.example.snapshot_system.entity.SysUser;
import com.example.snapshot_system.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.service.UserIpService;
import com.example.snapshot_system.util.EncryptUtils;
import com.example.snapshot_system.vo.PageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Console;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-10
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    MomentsMapper momentsMapper;


    @Override
    public PageVO<SysUser> getPage(SearchDTO searchDto) {
        LambdaQueryWrapper<SysUser> qw = new LambdaQueryWrapper<>();
        qw.like(SysUser::getPhone, searchDto.getSearchKey());
        PageVO<SysUser> page = page(searchDto, qw);
        return page;
    }

    @Override
    public void add(SysUser sysUser) {
        sysUser.setGmtCreate(new Date());
        sysUser.setGmtUpdate(new Date());
        this.save(sysUser);
    }

    @Override
    public boolean delete(String id) {
        UpdateWrapper<SysUser> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("phone",id).set("deleted", 1);
        boolean isDeleted = this.update(null, updateWrapper);
        return isDeleted;
    }

    @Override
    public boolean updateUser(String phone,String name,String gender,String profess,String introduction) {
        UpdateWrapper<SysUser> updateWrapper = new UpdateWrapper<>();
        if(!gender.equals(null)) {
            updateWrapper.eq("phone", phone).set("gender", gender);
        }
        updateWrapper.eq("phone",phone).set("gmt_update",new Date());
        if(!profess.equals(null)) {
            updateWrapper.eq("phone", phone).set("profess", profess);
        }
        if(!introduction.equals(null)) {
            updateWrapper.eq("phone", phone).set("introduction", introduction);
        }

        if(!name.equals(null)) {
            updateWrapper.eq("phone",phone).set("name",name);
        }

        //用户信息更新后对动态里的名字进行更新  （对动态评论1，公告评论，公告 更新）
        UpdateWrapper<Moments> updateWrapper1 = new UpdateWrapper<>();
        updateWrapper1.eq("phone_send",phone).set("name",name);
        momentsMapper.update(null,updateWrapper1);

        boolean isUpdated = this.update(null, updateWrapper);
        return isUpdated;
    }

    @Override
    public int login(String phone,String password) {

        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SysUser::getPhone, phone);
        List<SysUser> sysList = list(queryWrapper);
        //用户名不存在
        if(sysList.size() == 0){
            return -1;
        }

        //登陆成功
        if(EncryptUtils.encryptByMD5(password).equals(sysList.get(0).getPassword()) && !sysList.get(0).getDeleted()){
            Map<String,Object> m = new HashMap<String,Object>();
            m.put("userPhone",sysList.get(0).getPhone());
            return 1;
        }

        //登录失败
        return 0;
    }

    @Override
    public int register(String phone,String password,String confirm){
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SysUser::getPhone, phone);
        List<SysUser> sysList = list(queryWrapper);

        //用户名已经注册
        if(sysList.size() != 0){
            return -1;
        }

        //注册成功
        SysUser sysUser = new SysUser();
        sysUser.setPhone(phone);
        sysUser.setPassword(EncryptUtils.encryptByMD5(password));
        sysUser.setGmtCreate(new Date());
        sysUser.setGmtUpdate(new Date());
        this.save(sysUser);

        return 1;

    }

    @Override
    public SysUser getByPhone(String phone){
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SysUser::getPhone, phone);
        List<SysUser> sysList = list(queryWrapper);

        //获取失败，不存在
        if(sysList.size() == 0){
            return new SysUser();
        }

        //获取成功
        return sysList.get(0);
    }

    @Override
    public boolean updateAvatar(String phone,String avatar) {
        if(phone.equals(null) || avatar.equals(null)){
            return false;
        }

        //头像信息更新后对动态里的名字进行更新  （对动态评论1，公告评论，公告 更新）
        UpdateWrapper<Moments> updateWrapper1 = new UpdateWrapper<>();
        updateWrapper1.eq("phone_send",phone).set("avatar",avatar);
        momentsMapper.update(null,updateWrapper1);

        UpdateWrapper<SysUser> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("phone",phone).set("avatar",avatar);
        boolean isUpdated = this.update(null, updateWrapper);
        return isUpdated;
    }
}
