package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.entity.Moments;
import com.example.snapshot_system.entity.MomentsLikes;
import com.example.snapshot_system.mapper.MomentsLikesMapper;
import com.example.snapshot_system.mapper.MomentsMapper;
import com.example.snapshot_system.service.MomentsLikesService;
import com.example.snapshot_system.service.MomentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 朋友圈 服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Service
public class MomentsLikesServiceImpl extends ServiceImpl<MomentsLikesMapper, MomentsLikes> implements MomentsLikesService {

    @Override
    public boolean add(int id, String phone) {
        //信息不全
        if(phone.equals("") || phone.equals(null) || id<0){
            return false;
        }
        MomentsLikes momentsLikes = new MomentsLikes();
        momentsLikes.setId(Long.valueOf(id));
        momentsLikes.setPhone_called(phone);
        this.save(momentsLikes);
        return true;
    }

    @Override
    public boolean getIsMomentsLike(int id, String phone) {
        LambdaQueryWrapper<MomentsLikes> qw = new LambdaQueryWrapper<>();
        qw.eq(MomentsLikes::getId,id).eq(MomentsLikes::getPhone_called,phone);
        List<MomentsLikes> m = list(qw);
        //没有点赞信息
        if(m.size() == 0){
            return false;
        }

        //确认有点赞信息
        return true;
    }
}
