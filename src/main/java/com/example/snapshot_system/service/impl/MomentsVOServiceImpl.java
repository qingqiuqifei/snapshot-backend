package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.mapper.MomentsVOMapper;
import com.example.snapshot_system.service.MomentsVOService;
import com.example.snapshot_system.vo.MomentsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 朋友圈整合评论
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Service
public class MomentsVOServiceImpl extends ServiceImpl<MomentsVOMapper, MomentsVO> implements MomentsVOService {
    @Autowired
    MomentsVOMapper momentsVOMapper;

    @Override
    public List<MomentsVO> getMomentsVo(String phone) {
        List<MomentsVO> momentsVO = momentsVOMapper.getMomentsVo(phone);
        return momentsVO;
    }
}
