package com.example.snapshot_system.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.example.snapshot_system.service.OssService;
import com.example.snapshot_system.util.ConstantPropertiesUtils;
import javafx.util.Pair;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class OssServiceImpl implements OssService {

    @Override
    public String uploadFileAvatar(InputStream inputStream, String module, String originalFilename) {
        //工具类获取值
        String endpoint = ConstantPropertiesUtils.END_POINT;
        String accessKeyId = ConstantPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.ACCESS_KEY_SECRET;
        String bucketName = ConstantPropertiesUtils.BUCKET_NAME;


        //创建OSS实例
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        String folder = new DateTime().toString("yyyy/MM/dd");
        String fileName = UUID.randomUUID().toString();
        String fileExtension = originalFilename.substring(originalFilename.lastIndexOf("."));
        // oss中的文件夹名
        String objectName = module + "/" + folder + "/" + fileName + fileExtension;

        // 创建上传文件的元信息，可以通过文件元信息设置HTTP header(设置了才能通过返回的链接直接访问)。
        //如果不设置，直接访问url会自行下载图片，看各位自己选择
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("image/jpg");

        ossClient.putObject(bucketName, objectName, inputStream,objectMetadata);

        // 关闭OSSClient。
        ossClient.shutdown();

        String url = "http://"+bucketName+"."+endpoint+"/"+objectName;
        return url;
    }

    @Override
    public Pair<String, String> uploadFile(MultipartFile file) throws IOException {
        String endpoint = ConstantPropertiesUtils.END_POINT;
        String accessKeyId = ConstantPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.ACCESS_KEY_SECRET;
        String bucketName = ConstantPropertiesUtils.BUCKET_NAME;
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
// 获取文件名
        String fileName = file.getOriginalFilename();
// 获取文件后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
// 最后上传生成的文件名
        String finalFileName = System.currentTimeMillis() + "" + new SecureRandom().nextInt(0x0400) + suffixName;
// oss中的文件夹名
        String objectName = "20210721" + "/" + finalFileName;
// 创建上传文件的元信息，可以通过文件元信息设置HTTP header(设置了才能通过返回的链接直接访问)。
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("image/jpg");
// 文件上传
        ossClient.putObject(bucketName, objectName, new ByteArrayInputStream(file.getBytes()),objectMetadata);
// 设置URL过期时间为10年。
        Date expiration = new Date(System.currentTimeMillis() + 3600L * 1000 * 24 * 365 * 10);
        String url = ossClient.generatePresignedUrl(bucketName, objectName, expiration).toString();
        ossClient.shutdown();

        Pair<String, String> pair = new Pair<>(finalFileName, url);
        return pair;

    }
}
