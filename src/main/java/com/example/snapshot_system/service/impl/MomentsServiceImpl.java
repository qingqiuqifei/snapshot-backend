package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.snapshot_system.entity.Moments;
import com.example.snapshot_system.entity.SysUser;
import com.example.snapshot_system.mapper.MomentsMapper;
import com.example.snapshot_system.mapper.SysUserMapper;
import com.example.snapshot_system.service.MomentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.service.OssService;
import com.example.snapshot_system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 朋友圈 服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Service
public class MomentsServiceImpl extends ServiceImpl<MomentsMapper, Moments> implements MomentsService {
    @Autowired
    MomentsMapper momentsMapper;

    @Autowired
    SysUserMapper sysUserMapper;

    @Autowired
    SysUserService sysUserService;

    @Autowired
    OssServiceImpl ossService;


    @Override
    public List<Moments> getMoments(String phone_send) {
        QueryWrapper<Moments> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Moments::getPhone_send, phone_send);
        List<Moments> momentsList = list(queryWrapper);

        Collections.sort(momentsList, Comparator.comparing(Moments::getGmt_time));
        Collections.reverse(momentsList);
        return momentsList;
    }

    @Override
    public boolean add(String phone_send, String name, String content, String pic, String avatar) {
        //信息不全添加失败
        if(phone_send.equals(null) || content.equals(null)){
            return false;
        }

        name = sysUserService.getByPhone(phone_send).getName();

        Moments moments = new Moments();
        moments.setPhone_send(phone_send);
        moments.setName(name);
        moments.setContent(content);
        moments.setPic(pic);
        moments.setAvatar(avatar);
        moments.setGmtTime(new Date());
        this.save(moments);
        return true;
    }

    @Override
    public boolean deleteById(int id) {
        LambdaQueryWrapper<Moments> qw = new LambdaQueryWrapper<>();
        qw.eq(Moments::getId,id);
        int rows = momentsMapper.delete(qw);

        return rows == 0?false:true;
    }

    @Override
    public boolean addLikes(int id) {
        QueryWrapper<Moments> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Moments::getId,id);
        List<Moments> momentsList = list(queryWrapper);
        int toLikes = momentsList.get(0).getLikes();
        ++toLikes;

        UpdateWrapper<Moments> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",id).set("likes",toLikes);
        this.update(updateWrapper);
        return true;
    }
}
