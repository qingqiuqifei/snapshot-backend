package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.entity.SysUser;
import com.example.snapshot_system.entity.UserIp;
import com.example.snapshot_system.mapper.UserIpMapper;
import com.example.snapshot_system.service.UserIpService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserIpServiceImpl extends ServiceImpl<UserIpMapper, UserIp> implements UserIpService {
    @Override
    public String getIp(String phone) {
        QueryWrapper<UserIp> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(UserIp::getPhone, phone);
        List<UserIp> llist = list(queryWrapper);

        //返回用户的ip
        if(llist.size() == 0){
            return "0";
        }
        return llist.get(0).getIp();
    }

    @Override
    public boolean add(String phone, String ip) {
        LambdaQueryWrapper<UserIp> qw = new LambdaQueryWrapper<>();
        qw.eq(UserIp::getPhone,phone);
        List<UserIp> friendOwnList = list(qw);

        //有号码和ip
        if(friendOwnList.size() !=0){
            UpdateWrapper<UserIp> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("phone",phone).set("ip",ip);
          this.update(null, updateWrapper);
        }else{
            //没有号码和ip
            UserIp fr = new UserIp();
            fr.setPhone(phone);
            fr.setIp(ip);
            this.save(fr);
        }
//            UserIp fr = new UserIp();
//            fr.setPhone(phone);
//            fr.setIp(ip);
//            this.save(fr);
        return true;


    }
}
