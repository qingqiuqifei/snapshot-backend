package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.snapshot_system.entity.MomentsComments;
import com.example.snapshot_system.mapper.MomentsCommentsMapper;
import com.example.snapshot_system.service.MomentsCommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snapshot_system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 朋友圈的评论 服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Service
public class MomentsCommentsServiceImpl extends ServiceImpl<MomentsCommentsMapper, MomentsComments> implements MomentsCommentsService {
    @Autowired
    private MomentsCommentsMapper momentsCommentsMapper;

    @Autowired
    private SysUserService sysUserService;

    @Override
    public boolean add(int id,String phone_called, String name, String content) {
        LambdaQueryWrapper<MomentsComments> qw = new LambdaQueryWrapper<>();

        //信息不全
        if(     id <= 0 ||
                phone_called.equals(null) ||
                content.equals(null)){
            return false;
        }

        MomentsComments fr = new MomentsComments();
        fr.setId(Long.valueOf(id));
        fr.setPhone_called(phone_called);
        fr.setName(name);
        fr.setContent(content);
        fr.setAvatar(sysUserService.getByPhone(phone_called).getAvatar());
        this.save(fr);
        return true;
    }

    @Override
    public boolean deleteById(int unique_id) {
        LambdaQueryWrapper<MomentsComments> qw = new LambdaQueryWrapper<>();
        qw.eq(MomentsComments::getUnique_id,unique_id);
        int rows = momentsCommentsMapper.delete(qw);

        return rows == 0?false:true;
    }

    @Override
    public List<MomentsComments> getById(int id) {
        QueryWrapper<MomentsComments> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(MomentsComments::getId, id);
        List<MomentsComments> MomentsCommentsList = list(queryWrapper);
        return MomentsCommentsList;
    }
}
