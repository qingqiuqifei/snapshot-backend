package com.example.snapshot_system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.snapshot_system.entity.FriendApply;
import com.example.snapshot_system.entity.FriendOwn;
import com.example.snapshot_system.mapper.FriendApplyMapper;
import com.example.snapshot_system.service.FriendApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 申请好友的列表 服务实现类
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Service
public class FriendApplyServiceImpl extends ServiceImpl<FriendApplyMapper, FriendApply> implements FriendApplyService {
    @Autowired
    FriendApplyMapper friendApplyMapper;

    @Override
    public List<FriendApply> getFriendOwn(String phone) {
        QueryWrapper<FriendApply> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(FriendApply::getMyself_phone, phone);
        List<FriendApply> friendOwnList = list(queryWrapper);
        return friendOwnList;
    }

    @Override
    public boolean addFriendApply(String phone, String friendPhone) {
        LambdaQueryWrapper<FriendApply> qw = new LambdaQueryWrapper<>();
        qw.eq(FriendApply::getMyself_phone,phone).eq(FriendApply::getApply_phone,friendPhone);
        List<FriendApply> friendOwnList = list(qw);

        //已经有好友了
        if(friendOwnList.size() !=0){
            return false;
        }

        FriendApply fr = new FriendApply();
        fr.setMyself_phone(phone);
        fr.setApply_phone(friendPhone);
        this.save(fr);
        return true;
    }

    @Override
    public boolean deleteFriendApply(String phone, String friendPhone) {
        LambdaQueryWrapper<FriendApply> qw = new LambdaQueryWrapper<>();
        qw.eq(FriendApply::getMyself_phone,phone).eq(FriendApply::getApply_phone,friendPhone);
        int rows = friendApplyMapper.delete(qw);

        return rows == 0?false:true;
    }
}
