package com.example.snapshot_system.service;

import javafx.util.Pair;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;


/**
 * <p>
 * 上传图片资源类
 * </p>
 *
 * @author jin
 * @since 2021-07-16
 */
public interface OssService {
    /**
     * ?
     * @param inputStream ?
     * @param module ?
     * @param originalFilename ?
     * @return String
     */
    String uploadFileAvatar(InputStream inputStream, String module, String originalFilename);

    /**
     * ?
     * @param file ?
     * @return String
     * @throws IOException 异常
     */
    Pair<String, String> uploadFile(MultipartFile file) throws IOException;
}
