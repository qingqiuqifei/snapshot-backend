package com.example.snapshot_system.service;

import com.example.snapshot_system.dto.SearchDTO;
import com.example.snapshot_system.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.snapshot_system.vo.PageVO;

/**
 * <p>
 * 公告 服务类
 * </p>
 *
 * @author jin
 * @since 2021-07-16
 */
public interface NoticeService extends ISuperService<Notice> {

    /**
     *
     * @param searchDto
     * @return Notice
     */
    PageVO<Notice> getPage(SearchDTO searchDto);

    /**
     *
     * @param notice
     */
    boolean add(Notice notice);

    boolean deleteByTitle(String title);

    boolean updateNotice(Notice notice);

}
