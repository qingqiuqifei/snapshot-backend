package com.example.snapshot_system.service;

import com.example.snapshot_system.entity.UserIp;

public interface UserIpService extends ISuperService<UserIp> {
    String getIp(String phone);


    //如果有旧的号码,ip就会更新，如果没有号码就会自动添加
    boolean add(String phone,String ip);
}
