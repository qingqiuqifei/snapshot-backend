package com.example.snapshot_system.dto;

import lombok.Data;

@Data
public class SearchDTO {
    int current;
    int size;
    String searchKey;
    String pageType;
    String instanceType;

    public String getSearchKey() {
        return searchKey;
    }

    public Integer getCurrent() {
        return current;
    }

    public Integer getSize() {
        return size;
    }
}
