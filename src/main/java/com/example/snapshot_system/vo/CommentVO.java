package com.example.snapshot_system.vo;

import com.example.snapshot_system.entity.Comment;
import lombok.Data;

/**
 * <p>
 * 会话信息整合头像，姓名 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */

@Data
public class CommentVO extends Comment {
    private String phone_send_name;

    private String phone_send_avatar;

    private String phone_received_name;

    private String phone_received_avatar;
}
