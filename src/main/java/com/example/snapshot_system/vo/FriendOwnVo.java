package com.example.snapshot_system.vo;


import com.example.snapshot_system.entity.Comment;
import com.example.snapshot_system.entity.FriendOwn;
import lombok.Data;


//访问好友列表时返回头像和姓名
@Data
public class FriendOwnVo extends FriendOwn {
    private String avatar;

    private String name;
}
