package com.example.snapshot_system.vo;


import com.example.snapshot_system.entity.Moments;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class MomentsVO extends Moments {
    private int commentsCounts;

    private int isLiked;
}

