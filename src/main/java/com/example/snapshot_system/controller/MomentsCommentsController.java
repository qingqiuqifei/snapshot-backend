package com.example.snapshot_system.controller;


import com.alibaba.fastjson.JSON;
import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.common.ServerResponse;
import com.example.snapshot_system.entity.NoticeComments;
import com.example.snapshot_system.service.MomentsCommentsService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 朋友圈的评论 前端控制器
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@RestController
@RequestMapping("/moments_comments")
@Api(tags = "动态的评论")
public class MomentsCommentsController {
    @Autowired
    private MomentsCommentsService momentsCommentsService;

    @GetMapping("/getById")
    @ApiOperation(value = "获取动态的评论", notes = "获取动态的评论", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "动态id", paramType = "query", required = true, dataType = "int")
    })
    public ServerResponse getById(int id) {
        //用户不存在
        if(id<0){ return  ServerResponse.dataResponse(HttpStatus.MOMENTS_ERROR);}

        else{
            //操作成功
            return ServerResponse.successResponse(momentsCommentsService.getById(id));
        }

    }

    @PostMapping("/add")
    @ApiOperation(value = "添加动态评论", notes = "添加动态评论", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "公告id", paramType = "query",  dataType = "int"),
            @ApiImplicitParam(name = "phone_called", value = "回复者的手机号", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "name", value = "回复者的姓名", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "content", value = "回复内容", paramType = "query",  dataType = "string"),
    })
    public ServerResponse add(@ApiParam(name = "添加动态评论", value = "传入添加的业务条件", required = true) @RequestBody String noticeCommentsJson) {
        //判断公告信息是否完整
        int id = JSON.parseObject(noticeCommentsJson).getInteger("id");
        String phone_called = JSON.parseObject(noticeCommentsJson).getString("phone_called");
        String name = JSON.parseObject(noticeCommentsJson).getString("name");
        String content = JSON.parseObject(noticeCommentsJson).getString("content");

        if(id <= 0 || phone_called.equals(null) || content.equals(null) || name.equals(null)){
            return ServerResponse.dataResponse(HttpStatus.NOTICE_ERROR);
        }

        //操作成功
        if(momentsCommentsService.add(id, phone_called, name,content)){return ServerResponse.dataResponse(HttpStatus.SUCCESS);}

        //操作失败
        return ServerResponse.dataResponse(HttpStatus.NOTICE_ERROR);
    }

    @DeleteMapping("/deleteByID")
    @ApiOperation(value = "删除动态评论", notes = "删除动态评论", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "unique_id", value = "动态评论unique_id", paramType = "query", required = true, dataType = "int")
    })
    public ServerResponse deleteById(int unique_id) {
        //操作成功
        if(momentsCommentsService.deleteById(unique_id)){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }

        //操作失败
        return ServerResponse.dataResponse(HttpStatus.NOTICE_NOEXIST);
    }
}

