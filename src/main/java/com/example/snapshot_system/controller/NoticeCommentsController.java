package com.example.snapshot_system.controller;


import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.common.ServerResponse;
import com.example.snapshot_system.entity.Notice;
import com.example.snapshot_system.entity.NoticeComments;
import com.example.snapshot_system.service.NoticeCommentsService;
import com.example.snapshot_system.service.SysUserService;
import com.example.snapshot_system.util.CheckUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 新闻的回复 前端控制器
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@RestController
@RequestMapping("/notice_comments")
@Api(tags = "公告和新闻评论")
public class NoticeCommentsController {
    @Autowired
    private NoticeCommentsService noticeCommentsService;


    @GetMapping("/getById")
    @ApiOperation(value = "获取公告新闻评论", notes = "获取公告新闻评论", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "新闻公告id", paramType = "query", required = true, dataType = "int")
    })
    public ServerResponse getByPhone(int id) {
        //用户不存在
        if(id<0){ return  ServerResponse.dataResponse(HttpStatus.NOTICE_COMMENTSID_ERROR);}

        //操作成功
        return ServerResponse.successResponse(noticeCommentsService.getById(id));

    }

    @PostMapping("/add")
    @ApiOperation(value = "添加公告评论", notes = "添加公告评论", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "公告id", paramType = "query",  required = true,  dataType = "int"),
            @ApiImplicitParam(name = "phone_called", value = "回复者的手机号", paramType = "query", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "content", value = "回复内容", paramType = "query",  required = true,  dataType = "string"),
    })
    public ServerResponse add(@ApiParam(name = "添加公告评论", value = "传入添加的业务条件", required = true)NoticeComments noticeComments) {
        //判断公告信息是否完整
        if(noticeComments.getId().equals(null) ||
                noticeComments.getPhone_called().equals(null) ||
                noticeComments.getContent().equals(null)){
            return ServerResponse.dataResponse(HttpStatus.NOTICE_COMMENTS_ERROR);
        }

        //操作成功
        if(noticeCommentsService.add(noticeComments)){return ServerResponse.dataResponse(HttpStatus.SUCCESS);}

        //操作失败
        return ServerResponse.dataResponse(HttpStatus.NOTICE_ERROR);
    }

    @DeleteMapping("/deleteByID")
    @ApiOperation(value = "删除公告新闻评论", notes = "删除公告新闻评论", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "unique_id", value = "公告评论unique_id", paramType = "query", required = true, dataType = "int")
    })
    public ServerResponse deleteById(int unique_id) {
        //操作成功
        if(noticeCommentsService.deleteById(unique_id)){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }

        //操作失败
        return ServerResponse.dataResponse(HttpStatus.NOTICE_NOEXIST);
    }
}

