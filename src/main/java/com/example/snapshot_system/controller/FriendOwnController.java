package com.example.snapshot_system.controller;


import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.common.ServerResponse;
import com.example.snapshot_system.entity.FriendOwn;
import com.example.snapshot_system.service.FriendOwnService;
import com.example.snapshot_system.service.FriendOwnVoService;
import com.example.snapshot_system.service.SysUserService;
import com.example.snapshot_system.util.CheckUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * <p>
 * 好友列表 前端控制器
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@RestController
@RequestMapping("/friend_own")
@Api(tags = "用户好友")
public class FriendOwnController {
    @Autowired
    private FriendOwnService friendOwnService;

    @Autowired
    private FriendOwnVoService friendOwnVoService;

    public static HashMap<String, String> map = new HashMap<>();

    @GetMapping("/getByOwnPhone")
    @ApiOperation(value = "获取用户好友列表", notes = "获取用户好友列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse getFriendOwn(String phone) { ;
        //用户不存在
        if(!CheckUtils.checkPhone(phone)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        //操作成功
        return ServerResponse.successResponse(friendOwnVoService.getFriendOwnVo(phone));

    }

    @PostMapping("/addFriend")
    @ApiOperation(value = "添加用户好友", notes = "添加用户好友", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string"),
            @ApiImplicitParam(name = "friendPhone", value = "好友电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse addFriend(String phone , String friendPhone) {
        //用户不存在
        if(!CheckUtils.checkPhone(phone) || !CheckUtils.checkPhone(friendPhone)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        //操作成功
        if(friendOwnService.addFriend(phone , friendPhone)){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }
        return ServerResponse.dataResponse(HttpStatus.USERNAME_VERIFICATION_ERROR);
    }

    @DeleteMapping("/deleteFriend")
    @ApiOperation(value = "删除用户好友", notes = "删除用户好友", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string"),
            @ApiImplicitParam(name = "friendPhone", value = "删除好友的电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse deleteFriend(String phone , String friendPhone) {
        //用户不存在
        if(!CheckUtils.checkPhone(phone) || !CheckUtils.checkPhone(friendPhone)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        //操作成功
        if(friendOwnService.deleteFriend(phone , friendPhone)){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }

        //删除好友不存在
        return ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);
    }
}

