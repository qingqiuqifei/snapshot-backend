package com.example.snapshot_system.controller;


import com.example.snapshot_system.common.ServerResponse;
import com.example.snapshot_system.dto.SearchDTO;
import com.example.snapshot_system.entity.SysUser;
import com.example.snapshot_system.service.OssService;
import com.example.snapshot_system.service.SysUserService;
import com.example.snapshot_system.service.UserIpService;
import com.example.snapshot_system.util.CheckUtils;
import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.vo.PageVO;
import com.example.snapshot_system.util.JWTUtils;
import io.swagger.annotations.*;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jin
 * @since 2021-07-10
 */
@RestController
@RequestMapping("/sys-user")
@Api(tags = "用户管理")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private OssService ossService;
    @Autowired
    private UserIpService userIpService;

    @GetMapping("/list")
    @ApiOperation(value = "列表", notes = "分页查询列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "页码", paramType = "query", defaultValue = "1", dataType = "int"),
            @ApiImplicitParam(name = "size", value = "每页条数", paramType = "query", defaultValue = "10", dataType = "int"),
            @ApiImplicitParam(name = "searchKey", value = "查询条件(电话号码)", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "pageType", value = "页面类型", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "instanceType", value = "页面实例类型", paramType = "query", dataType = "string"),
    })

    public PageVO<SysUser> getPage(@ApiParam(name = "查询", value = "传入查询的业务条件", required = true) SearchDTO searchDTO) {
        PageVO<SysUser> page = sysUserService.getPage(searchDTO);
        return page;
    }

    @PostMapping("/add")
    @ApiOperation(value = "添加用户信息", notes = "添加用户信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query",  required = true,  dataType = "string"),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "query", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "avatar", value = "头像", paramType = "query", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "name", value = "姓名", paramType = "query",  required = true,  dataType = "string"),
            @ApiImplicitParam(name = "gender", value = "性别", paramType = "query", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "profess", value = "职业", paramType = "query", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "identify", value = "身份", paramType = "query", required = true,  dataType = "boolean"),
            @ApiImplicitParam(name = "introduction", value = "个人简介", paramType = "query",  dataType = "string"),
    })
    public void add(@ApiParam(name = "用户信息", value = "传入添加的业务条件", required = true) SysUser sysUser) {
        sysUserService.add(sysUser);
    }

    @DeleteMapping("/deleteByPhone")
    @ApiOperation(value = "删除用户信息", notes = "删除用户信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse deleteById(String phone) {
        //用户不存在
        if(!CheckUtils.checkPhone(phone)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        if(!sysUserService.delete(phone)){
            return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);
        }

        //操作成功
        return ServerResponse.dataResponse(HttpStatus.SUCCESS);
    }

    @PostMapping("/updateByPhone")
    @ApiOperation(value = "更新用户信息", notes = "更新用户信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "name", value = "姓名", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "gender", value = "性别", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "profess", value = "职业", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "introduction", value = "个人简介", paramType = "query",  dataType = "string"),
    })
    public ServerResponse updateById(@ApiParam(name = "用户信息", value = "传入修改的用户条件", required = true) @RequestBody String sysUserJson) {
        String phone = JSON.parseObject(sysUserJson).getString("phone");
        String name = JSON.parseObject(sysUserJson).getString("name");
        String gender = JSON.parseObject(sysUserJson).getString("gender");
        String profess = JSON.parseObject(sysUserJson).getString("profess");
        String introduction = JSON.parseObject(sysUserJson).getString("introduction");
        if(phone.equals(null) || name.equals(null) || gender.equals(null) || profess.equals(null) || introduction.equals(null) || !CheckUtils.checkGender(gender)){
            return ServerResponse.dataResponse(HttpStatus.USER_UPDATE_EROOR);
        }
        if(sysUserService.updateUser(phone,name,gender,profess,introduction)){
            return ServerResponse.successResponse(sysUserService.getByPhone(phone));
        }
        return ServerResponse.dataResponse(HttpStatus.USER_UPDATE_EROOR);
    }

    @PostMapping("/login")
    @ApiOperation(value = "用户登录", notes = "用户登录", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query",  required = true,  dataType = "string"),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "query", required = true,  dataType = "string"),
    })
    public ServerResponse login(String phone, String password, HttpServletRequest request)
    {
        //用户名校验错误
        if(!CheckUtils.checkPhone(phone)){ return  ServerResponse.dataResponse(HttpStatus.USERNAME_VERIFICATION_ERROR);}

        //密码长度校验错误
        if(!CheckUtils.checkPassword(password)){ return  ServerResponse.dataResponse(HttpStatus.PASSWORD_LENGTH_ERROR);}

        //未注册账号
        if(sysUserService.login(phone,password) == -1){ return ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS); }

        //密码错误
        if(sysUserService.login(phone,password) == 0){ return  ServerResponse.dataResponse(HttpStatus.USER_PASSWORD_ERROR); }

        userIpService.add(phone,request.getRemoteAddr());


        //返回token
        Map<String, Object> map = new HashMap<>();
        String token = JWTUtils.generateTokenById(sysUserService.getByPhone(phone).getId(),
                sysUserService.getByPhone(phone).getPhone());
        map.put("login", sysUserService.getByPhone(phone));
        map.put("token", token);

        return ServerResponse.successResponse(map);

    }

    @PostMapping("/register")
    @ApiOperation(value = "用户注册", notes = "用户注册", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query",  required = true,  dataType = "string"),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "query", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "confirm", value = "确认密码", paramType = "query", required = true,  dataType = "string")
    })
    public ServerResponse register(String phone, String password, String confirm)
    {
        //用户名校验错误
        if(!CheckUtils.checkPhone(phone)){ return  ServerResponse.dataResponse(HttpStatus.USERNAME_VERIFICATION_ERROR);}

        //密码长度校验错误
        if(!CheckUtils.checkPassword(password)){ return  ServerResponse.dataResponse(HttpStatus.PASSWORD_LENGTH_ERROR);}

        //密码错误
        if(!password.equals(confirm)){
            return  ServerResponse.dataResponse(HttpStatus.USER_PASSWORD_ERROR); }

        //已注册账号
        if(sysUserService.register(phone,password,confirm) == -1){ return ServerResponse.dataResponse(HttpStatus.PHONE_ALREADY_REGISTERED); }
        else{
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }
    }

    @GetMapping("/getByPhone")
    @ApiOperation(value = "获取用户信息", notes = "获取用户信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse getByPhone(String phone) {
        //用户不存在
        if(!CheckUtils.checkPhone(phone)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        if(sysUserService.getByPhone(phone).getDeleted() == null || sysUserService.getByPhone(phone).getDeleted() == true){
            return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);
        }
        else{
            //操作成功
            return ServerResponse.successResponse(sysUserService.getByPhone(phone));
        }

    }

    @PostMapping("/updateAvatar")
    @ApiOperation(value = "更新头像", notes = "更新头像", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse upDateAvatar(String phone,@RequestParam("file") MultipartFile file) throws IOException {
        //用户名校验错误
        if(!CheckUtils.checkPhone(phone) || file.isEmpty()){ return  ServerResponse.dataResponse(HttpStatus.USERNAME_VERIFICATION_ERROR);}
        Pair<String,String> pair = ossService.uploadFile(file);

        sysUserService.updateAvatar(phone,pair.getKey());
        return ServerResponse.successResponse(pair.getKey());

    }
}

