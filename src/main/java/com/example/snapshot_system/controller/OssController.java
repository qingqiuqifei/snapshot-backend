package com.example.snapshot_system.controller;

import com.example.snapshot_system.service.OssService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author jin
 */
@RestController
@CrossOrigin
@RequestMapping("/oss")
@Api(tags = "上传图片")
public class OssController {

    @Autowired
    private OssService ossService;

    /**
     * 上传图片的方法
     * @param file 文件
     * @param module ?
     * @return int
     */
    @PostMapping("policy/uploadPic")
    @ResponseBody
    public String uploadOssFile(@RequestParam("file") MultipartFile file, String module) {
        //获取上传文件 MultipartFile
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String url = ossService.uploadFileAvatar(inputStream,module,file.getOriginalFilename());

        return url;
    }

    /**
     * 获取上传文件
     * @param file 文件
     * @return String
     * @throws IOException 异常
     */
    @PostMapping("/fileoss")
    @ResponseBody
    public String uploadOssFile(MultipartFile file) throws IOException {
        //key是filename,value是url
        String url = ossService.uploadFile(file).getKey();
        return url;
    }
}

