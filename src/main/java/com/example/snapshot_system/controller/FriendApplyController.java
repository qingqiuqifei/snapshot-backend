package com.example.snapshot_system.controller;


import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.common.ServerResponse;
import com.example.snapshot_system.service.FriendApplyService;
import com.example.snapshot_system.service.FriendOwnService;
import com.example.snapshot_system.util.CheckUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 申请好友的列表 前端控制器
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@RestController
@RequestMapping("/friend_apply")
@Api(tags = "申请用户好友")
public class FriendApplyController {
    @Autowired
    private FriendApplyService friendApplyService;

    @GetMapping("/getApplyFriend")
    @ApiOperation(value = "获取用户好友列表", notes = "获取用户好友列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse getApplyFriend(String phone) {
        //用户不存在
        if(!CheckUtils.checkPhone(phone)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        //操作成功
        return ServerResponse.successResponse(friendApplyService.getFriendOwn(phone));
    }

    @PostMapping("/addFriendApply")
    @ApiOperation(value = "添加申请用户好友", notes = "添加申请用户好友", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string"),
            @ApiImplicitParam(name = "friendPhone", value = "申请好友电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse addFriendApply(String phone , String friendPhone) {
        //用户不存在
        if(!CheckUtils.checkPhone(phone) || !CheckUtils.checkPhone(friendPhone)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        //操作成功
        if(friendApplyService.addFriendApply(phone , friendPhone)){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }
        return ServerResponse.dataResponse(HttpStatus.USERNAME_VERIFICATION_ERROR);
    }

    @DeleteMapping("/deleteFriendApply")
    @ApiOperation(value = "删除申请用户好友", notes = "删除申请用户好友", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "电话号码", paramType = "query", required = true, dataType = "string"),
            @ApiImplicitParam(name = "friendPhone", value = "删除申请好友的电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse deleteFriendApply(String phone , String friendPhone) {
        //用户不存在
        if(!CheckUtils.checkPhone(phone) || !CheckUtils.checkPhone(friendPhone)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        //操作成功
        if(friendApplyService.deleteFriendApply(phone , friendPhone)){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }

        //删除好友不存在
        return ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);
    }
}

