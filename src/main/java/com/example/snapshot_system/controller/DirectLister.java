package com.example.snapshot_system.controller;

import com.example.snapshot_system.config.RabbitMqConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.amqp.core.Message;

@Component
public class DirectLister {
    @RabbitListener(queues = RabbitMqConfig.DIRECT_QUEUE)
    public void Lister(Message message){
        byte[] body = message.getBody();
        System.err.println("接受到的消息体："+new String(body));
    }
}
