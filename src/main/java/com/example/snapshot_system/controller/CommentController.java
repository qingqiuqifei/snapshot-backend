package com.example.snapshot_system.controller;


import com.alibaba.fastjson.JSON;
import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.common.ServerResponse;
import com.example.snapshot_system.mapper.CommentVOMapper;
import com.example.snapshot_system.service.CommentService;
import com.example.snapshot_system.service.UserIpService;
import com.example.snapshot_system.util.CheckUtils;
import com.example.snapshot_system.util.MyClient;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * <p>
 * 会话信息表 前端控制器
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@RestController
@RequestMapping("/comment")
@Api(tags = "会话信息")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @Autowired
    private CommentVOMapper commentVOMapper;

    @Autowired
    private UserIpService userIpService;


    @GetMapping("/getComments")
    @ApiOperation(value = "获取会话信息", notes = "获取会话信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "用户手机号", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse getByPhone(String phone, HttpServletRequest request) throws IOException {


        //用户名校验错误
        if(!CheckUtils.checkPhone(phone)){ return  ServerResponse.dataResponse(HttpStatus.USERNAME_VERIFICATION_ERROR);}

        //操作成功
        return ServerResponse.successResponse(commentVOMapper.getAllComentVo(phone));

    }

    @GetMapping("/getCommentsFriend")
    @ApiOperation(value = "获取与好友一对一的会话信息,ID是偶数为接收方", notes = "获取与好友一对一的会话信息,ID是偶数为接收方", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone_send", value = "发送者用户手机号", paramType = "query", required = true, dataType = "string"),
            @ApiImplicitParam(name = "phone_received", value = "接收者用户手机号", paramType = "query", required = true, dataType = "string"),
    })
    public ServerResponse getByPhoneFriend(String phone_send, String phone_received) {
        //用户名校验错误
        if(!CheckUtils.checkPhone(phone_send) || !CheckUtils.checkPhone(phone_received)){ return  ServerResponse.dataResponse(HttpStatus.USERNAME_VERIFICATION_ERROR);}

        //操作成功
        return ServerResponse.successResponse(commentService.getCommentsFriend(phone_send,phone_received));

    }

    @PostMapping("/add")
    @ApiOperation(value = "添加会话信息", notes = "添加会话信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone_send", value = "发送信息用户手机号", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "phone_received", value = "收到信息用户手机号", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "content", value = "会话内容", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "isText", value = "是否为图片(0为文字，1为图片)", paramType = "query", dataType = "int")
    })
    public ServerResponse add(@ApiParam(name = "添加会话评论", value = "传入添加的业务条件", required = true) @RequestBody String commentsJson,HttpServletRequest request) throws IOException {
        //判断会话信息是否完整
        String phone_send = JSON.parseObject(commentsJson).getString("phone_send");
        String phone_received = JSON.parseObject(commentsJson).getString("phone_received");
        String content = JSON.parseObject(commentsJson).getString("content");
        int isText = JSON.parseObject(commentsJson).getInteger("isText");

        if(userIpService.getIp(phone_received).equals("0")){
            return ServerResponse.dataResponse(HttpStatus.COMMENTS_ERROR);
        }

        //会话信息不完整
        if(!CheckUtils.checkPhone(phone_send) || !CheckUtils.checkPhone(phone_received) || content.equals(null) || isText<0 || isText>1){
            return ServerResponse.dataResponse(HttpStatus.COMMENTS_ERROR);
        }
        //保存成功
        if(commentService.add(phone_send,phone_received,content,isText)){
            // 通过printWriter 来向服务器发送消息(注意获取phone_received的ip地址)

//            Socket client = new Socket(request.getRemoteAddr(), 8088);
            Socket client = new Socket(userIpService.getIp(phone_received), 8088);
            System.out.println("aaa"+client.toString());
            PrintWriter printWriter = new PrintWriter(client.getOutputStream());
            System.out.println("连接已建立...");
            printWriter.println("钱康宇儿子");
            printWriter.flush();

            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }
        //保存失败
        return ServerResponse.dataResponse(HttpStatus.COMMENTS_ERROR);
    }

    @DeleteMapping("/deleteByID")
    @ApiOperation(value = "删除会话信息", notes = "删除会话信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会话信息id", paramType = "query", required = true, dataType = "int")
    })
    public ServerResponse deleteById(int id) {
        //操作成功
        if(commentService.deleteById(id)){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }

        //操作失败
        return ServerResponse.dataResponse(HttpStatus.COMMENTS_ERROR);
    }

    @GetMapping("/getIsCommentsRead")
    @ApiOperation(value = "获取会话信息读取状态(true为未读，false为已读)", notes = "获取会话信息读取状态(true为未读，false为已读)", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone", value = "用户手机号", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse getIsCommentsRead(String phone) {
        //用户名校验错误
        if(!CheckUtils.checkPhone(phone)){ return  ServerResponse.dataResponse(HttpStatus.USERNAME_VERIFICATION_ERROR);}

        //操作成功
        return ServerResponse.successResponse(commentService.getIsCommentsRead(phone));

    }
}

