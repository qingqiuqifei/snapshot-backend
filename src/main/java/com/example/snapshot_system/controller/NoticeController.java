package com.example.snapshot_system.controller;


import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.common.ServerResponse;
import com.example.snapshot_system.dto.SearchDTO;
import com.example.snapshot_system.entity.Notice;
import com.example.snapshot_system.entity.SysUser;
import com.example.snapshot_system.service.NoticeService;
import com.example.snapshot_system.service.SysUserService;
import com.example.snapshot_system.vo.PageVO;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 公告 前端控制器
 * </p>
 *
 * @author jin
 * @since 2021-07-16
 */
@RestController
@RequestMapping("/notice")
@Api(tags = "公告和新闻管理")
public class NoticeController {
    @Autowired
    private NoticeService noticeService;

    @GetMapping("/list")
    @ApiOperation(value = "列表", notes = "分页查询列表", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "页码", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "size", value = "每页条数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "searchKey", value = "查询条件", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "pageType", value = "页面类型", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "instanceType", value = "页面实例类型", paramType = "query", dataType = "string"),
    })

    public PageVO<Notice> getPage(@ApiParam(name = "查询", value = "传入查询的业务条件", required = true) SearchDTO searchDTO) {
        PageVO<Notice> page = noticeService.getPage(searchDTO);
        return page;
    }

    @PostMapping("/add")
    @ApiOperation(value = "添加公告信息", notes = "添加公告信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "标题", paramType = "query",  required = true,  dataType = "string"),
            @ApiImplicitParam(name = "content", value = "新闻内容", paramType = "query", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "name", value = "发布者", paramType = "query",  required = true,  dataType = "string"),
            @ApiImplicitParam(name = "avatar", value = "图片", paramType = "query", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "likes", value = "点赞数", paramType = "query", required = true,  dataType = "int"),
    })
    public ServerResponse add(@ApiParam(name = "公告信息", value = "传入添加的业务条件", required = true) Notice notice) {
        //判断公告信息是否完整
        if(notice.getTitle().equals(null) ||
                notice.getContent().equals(null) ||
                notice.getName().equals(null)){
            return ServerResponse.dataResponse(HttpStatus.NOTICE_ERROR);
        }

        //操作成功
        if(noticeService.add(notice)){return ServerResponse.dataResponse(HttpStatus.SUCCESS);}

        //操作失败
        return ServerResponse.dataResponse(HttpStatus.NOTICE_ERROR);
    }

    @DeleteMapping("/deleteByTitle")
    @ApiOperation(value = "删除公告信息", notes = "删除公告信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "公告标题", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse deleteById(String title) {
        //操作成功
        if(noticeService.deleteByTitle(title)){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }

        //操作失败
        return ServerResponse.dataResponse(HttpStatus.NOTICE_NOEXIST);
    }

    @PostMapping("/update")
    @ApiOperation(value = "更新公告信息", notes = "更新公告信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "标题", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "content", value = "新闻内容", paramType = "query", dataType = "string"),
            @ApiImplicitParam(name = "name", value = "发布者", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "avatar", value = "图片", paramType = "query",  dataType = "string"),
            @ApiImplicitParam(name = "likes", value = "点赞数", paramType = "query",  dataType = "int"),
            @ApiImplicitParam(name = "id", value = "公告id", paramType = "query",  dataType = "long"),
    })
    public ServerResponse updateById(@ApiParam(name = "公告信息", value = "传入修改的公告信息", required = true) Notice notice) {
        //判断更新公告信息是否完整
        if(     notice.getTitle().equals(null) ||
                notice.getContent().equals(null) ||
                notice.getName().equals(null) ||
                notice.getAvatar().equals(null) ||
                notice.getId().equals(null)){
            return ServerResponse.dataResponse(HttpStatus.NOTICE_ERROR);
        }

        //操作成功
        if(noticeService.updateNotice(notice)){return ServerResponse.dataResponse(HttpStatus.SUCCESS);}

        //操作失败
        return ServerResponse.dataResponse(HttpStatus.NOTICE_NOEXIST);
    }
}

