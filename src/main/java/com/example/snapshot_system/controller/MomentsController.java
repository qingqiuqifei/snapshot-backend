package com.example.snapshot_system.controller;


import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.common.ServerResponse;
import com.example.snapshot_system.service.*;
import com.example.snapshot_system.util.CheckUtils;
import com.example.snapshot_system.util.JWTUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * <p>
 * 朋友圈 前端控制器
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@RestController
@RequestMapping("/moments")
@Api(tags = "朋友圈动态")
public class MomentsController {
    @Autowired
    private MomentsService momentsService;

    @Autowired
    private MomentsVOService momentsVOService;

    @Autowired
    private MomentsLikesService momentsLikesService;

    @Autowired
    private OssService ossService;

    @Autowired
    private SysUserService sysUserService;

    //改
    @GetMapping("/getMoments")
    @ApiOperation(value = "获取用户动态", notes = "获取用户动态", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone_send", value = "发送动态的电话号码", paramType = "query", required = true, dataType = "string")
    })
    public ServerResponse getFriendOwn(String phone_send) {
        //用户不存在
        if(!CheckUtils.checkPhone(phone_send)){ return  ServerResponse.dataResponse(HttpStatus.USER_ACCOUNT_NOT_EXISTS);}

        //操作成功
        return ServerResponse.successResponse(momentsService.getMoments(phone_send));

    }

    //改
    @PostMapping("/addMoments")
    @ApiOperation(value = "添加用户动态", notes = "添加用户动态", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phone_send", value = "发送人电话号码", paramType = "query", required = true, dataType = "string"),
            @ApiImplicitParam(name = "name", value = "发送人姓名", paramType = "query", required = true, dataType = "string"),
            @ApiImplicitParam(name = "content", value = "内容", paramType = "query", required = false, dataType = "string"),
    })
    public ServerResponse addMoments(String phone_send , String name, String content,@RequestParam(value = "file",required=false) MultipartFile file) throws IOException {
        //用户或内容不存在

        if(!CheckUtils.checkPhone(phone_send) || content.equals(null)){ return  ServerResponse.dataResponse(HttpStatus.MOMENTS_ERROR);}
        String pic;

        //如果没有文件设置默认图片
        if(file == null || file.toString().equals("")){
            pic = "1626937509257129.jpeg";
        }else{
            Pair<String,String> pair = ossService.uploadFile(file);
            pic = pair.getKey();
            //如果没有图片
            if(pic.equals(null) || pic.equals("")){
                pic = "1626937509257129.jpeg";
            }

        }
        //操作成功
        if(momentsService.add(phone_send,name,content,pic,sysUserService.getByPhone(phone_send).getAvatar())){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }
        return ServerResponse.dataResponse(HttpStatus.MOMENTS_ERROR);
    }

    @DeleteMapping("/deleteMoments")
    @ApiOperation(value = "删除用户动态", notes = "删除用户动态", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "动态id", paramType = "query", required = true, dataType = "String"),
    })
    public ServerResponse deleteById(String id) {
        //操作成功
        if(momentsService.deleteById(Integer.valueOf(id))){
            return ServerResponse.dataResponse(HttpStatus.SUCCESS);
        }

        //删除好友不存在
        return ServerResponse.dataResponse(HttpStatus.MOMENTS_ERROR);
    }

    //改
    @GetMapping("/getAll")
    @ApiOperation(value = "获取用户所有动态", notes = "获取所有动态", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
           @ApiImplicitParam(name = "phone", value = "判断是否点赞的电话号码", paramType = "query", required = true, dataType = "String"),
    })
    public ServerResponse getAll(String phone) {

        //操作成功
        return ServerResponse.successResponse(momentsVOService.getMomentsVo(phone));

    }

    @PostMapping("/addMomentsLikes")
    @ApiOperation(value = "点赞用户动态", notes = "点赞用户动态", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "动态", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = "phone_send", value = "点赞者电话号码", paramType = "query", required = true, dataType = "string"),
    })
    public ServerResponse addMomentsLikes(int id,String phone_send) {
        //用户或内容不存在
        if(!CheckUtils.checkPhone(phone_send) || id<0){ return  ServerResponse.dataResponse(HttpStatus.MOMENTS_ERROR);}


        //操作成功
        if(momentsLikesService.getIsMomentsLike(id,phone_send)){
            return ServerResponse.dataResponse(HttpStatus.MOMENTS_LIKES_ERROR);
        }
        momentsService.addLikes(id);
        momentsLikesService.add(id,phone_send);

        return ServerResponse.dataResponse(HttpStatus.SUCCESS);
    }

    @GetMapping("/getIsMomentsLike")
    @ApiOperation(value = "获取用户是否点赞(false未点赞，true已点赞)", notes = "获取用户是否点赞", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "动态id", paramType = "query", required = true, dataType = "int"),
        @ApiImplicitParam(name = "phone", value = "用户电话号码", paramType = "query", required = true, dataType = "string"),
    })
    public ServerResponse getIsMomentsLike(int id,String phone) {
        if(!CheckUtils.checkPhone(phone) || id<0){ return  ServerResponse.dataResponse(HttpStatus.MOMENTS_ERROR);}

        //操作成功
        return ServerResponse.successResponse(momentsLikesService.getIsMomentsLike(id,phone));

    }
}

