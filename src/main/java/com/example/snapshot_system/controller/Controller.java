package com.example.snapshot_system.controller;

import com.example.snapshot_system.config.RabbitMqConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class Controller {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @GetMapping("sendDirectMq")
    public String sendDirectMq(String msg){
        rabbitTemplate.convertAndSend(RabbitMqConfig.DIRECT_EXCHANGE,RabbitMqConfig.DIRECT_QUEUE,msg);
        return msg;
    }
}
