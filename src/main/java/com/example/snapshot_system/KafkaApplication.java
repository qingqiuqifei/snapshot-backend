//package com.example.snapshot_system;
//
//
//import com.alibaba.fastjson.JSON;
//import com.example.snapshot_system.entity.SysUser;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.ConfigurableApplicationContext;
//import org.springframework.kafka.annotation.EnableKafka;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.kafka.core.KafkaTemplate;
//import springfox.documentation.spring.web.json.Json;
//
//@SpringBootApplication
//public class KafkaApplication {
//
//    public static void main(String[] args) {
//
//        ConfigurableApplicationContext context = SpringApplication.run(KafkaApplication.class, args);
//
//        KafkaTemplate kafkaTemplate = context.getBean(KafkaTemplate.class);
//
//        SysUser sysUser = new SysUser();
//        sysUser.setName("dawd");
//        sysUser.setPhone("1335646");
//        String s = JSON.toJSONString(sysUser);
//
//        for (int i = 0; i < 10; i++) {
//            //调用消息发送类中的消息发送方法
//            kafkaTemplate.send("mytopic", s);
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    @KafkaListener(topics = {"mytopic"},groupId = "halburt-demo2")
//    public void consumer1(String message) {
//        System.out.println("consumer1收到消息:" + message);
//    }
//
//    @KafkaListener(topics = {"mytopic"} ,groupId = "halburt-demo")
//    public void consumer2(ConsumerRecord<?, ?> record) {
//        System.out.println("consumer2收到消息");
//        System.out.println("    topic" + record.topic());
//        System.out.println("    key:" + record.key());
//        String phone_called = JSON.parseObject((String)record.value()).getString("phone");
//        System.out.println("    value:"+phone_called);
//    }
//}
