package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 公告
 * </p>
 *
 * @author jin
 * @since 2021-07-16
 */
@Data
public class Notice implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 新闻内容
     */
    private String content;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmt_create;

    /**
     * 更新时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmt_update;

    /**
     * 发布者
     */
    private String name;

    /**
     * 图片
     */
    private String avatar;

    /**
     * 点赞数
     */
    private Integer likes;

    /**
     * 逻辑删除
     */
    private Boolean deleted;

    public void setGmtCreate(Date date) {
        this.gmt_create = date;
    }

    public void setGmtUpdate(Date date) {
        this.gmt_update = date;
    }
}
