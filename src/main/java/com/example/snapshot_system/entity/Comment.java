package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 会话信息表
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@TableName("comments")
@Data
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
      @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 发送者的手机号
     */
    private String phone_send;

    /**
     * 接收者的手机号
     */
    private String phone_received;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date create_time;

    /**
     * 内容
     */
    private String content;

    /**
     * 是否为图片(0为文字，1为图片)
     */
    private int isText;

    /**
     * 信息是否阅读(0未阅读，1已阅读)
     */
    private boolean isRead;

    /**
     * 是否为最新消息(1为最新)
     */
    private boolean isNewest;



    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

}
