package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 申请好友的列表
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Data
@TableName("friend_apply")
public class FriendApply{


    /**
     * 本人手机号
     */
    private String myself_phone;

    /**
     * 申请好友的手机号
     */
    private String apply_phone;

}
