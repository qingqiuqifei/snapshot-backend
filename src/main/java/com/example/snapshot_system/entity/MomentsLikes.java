package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 朋友圈的点赞
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Data
@TableName("moments_likes")
public class MomentsLikes implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 点赞者的手机号
     */
    private String phone_called;

    /**
     * 自增id
     * unique_id
     */
    @TableId(value = "unique_id", type = IdType.AUTO)
    private Long unique_id;
}
