package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 好友列表
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Data
@TableName("friend_own")
public class FriendOwn {

    /**
     * 本人手机号
     */
    private String myself_phone;

    /**
     * 好友手机号
     */
    private String friend_phone;

    public FriendOwn() {
    }
}
