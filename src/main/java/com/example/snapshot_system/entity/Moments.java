package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 朋友圈
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Data
@TableName("moments")
public class Moments implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 发送者的手机号
     */
    private String phone_send;

    /**
     * 发送者的姓名
     */
    private String name;

    /**
     * 内容
     */
    private String content;

    /**
     * 图片
     */
    private String pic;

    /**
     * 点赞数
     */
    private Integer likes;

    /**
     *创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmt_time;

    /**
     * 头像
     */
    private String avatar;

    public Moments(String phone_send, String content, String avatar) {
    }

    public void setGmtTime(Date date) {
        this.gmt_time= date;
    }

    public Moments(){

    }

}
