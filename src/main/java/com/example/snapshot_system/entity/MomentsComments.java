package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 朋友圈的评论
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Data
@TableName("moments_comments")
public class MomentsComments implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;

    /**
     * 回复者的手机号
     */
    private String phone_called;

    /**
     * 内容
     */
    private String content;

    /**
     * 姓名
     */
    private String name;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 自增id
     * unique_id
     */
    @TableId(value = "unique_id", type = IdType.AUTO)
    private Long unique_id;
}
