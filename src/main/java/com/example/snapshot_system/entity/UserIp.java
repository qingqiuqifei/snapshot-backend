package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * <p>
 * 用户好友对应ip
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Data
@TableName("user_ip")
public class UserIp {

    /**
     * 本人手机号
     */
    private String phone;

    /**
     * 对应ip
     */
    private String ip;

    public UserIp() {
    }
}
