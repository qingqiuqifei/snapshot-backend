package com.example.snapshot_system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 新闻的回复
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
@Data
@TableName("notice_comments")
public class NoticeComments implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * unique_id
     */
    @TableId(value = "unique_id", type = IdType.AUTO)
    private Long unique_id;

    /**
     * ID
     */
    private Long id;

    /**
     * 回复者的手机号
     */
    private String phone_called;

    /**
     * 内容
     */
    private String content;
}
