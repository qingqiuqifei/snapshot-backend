package com.example.snapshot_system.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p> Project: examArrangementSystem </p>
 * <p> Package: com.example.snapshot_system.common </p>
 * <p> FileName: HttpStatus <p>
 * <p> Description: Http状态码 <p>
 * <p> Created By IntelliJ IDEA </p>
 *
 * @author jin
 * @since 2020/7/19
 */

@Getter
@AllArgsConstructor
public enum HttpStatus {

    /*==================================================================
    //                        CustomStatus                             //
    ==================================================================*/

    /**
     * <p>SUCCESS</p>
     */
    SUCCESS(10000, "SUCCESS", "成功"),

    /**
     * <p>CLIENT_ERROR</p>
     */
    CLIENT_ERROR(20000, "CLIENT_ERROR", "客户端错误"),
    REGISTER_ERROR(20100, "REGISTER_ERROR", "注册错误"),
    USERNAME_VERIFICATION_ERROR(20101, "USERNAME_VERIFICATION_ERROR", "用户名校验错误"),
    USERNAME_ALREADY_EXISTS(20102, "USERNAME_ALREADY_EXISTS", "用户名已存在"),
    PHONE_FORMAT_ERROR(20103, "PHONE_FORMAT_ERROR", "电话格式错误"),
    PHONE_ALREADY_REGISTERED(20104, "EMAIL_ALREADY_REGISTERED", "电话已经注册"),
    PASSWORD_VERIFICATION_ERROR(20105, "PASSWORD_VERIFICATION_ERROR", "密码校验失败"),
    PASSWORD_LENGTH_ERROR(20106, "PASSWORD_LENGTH_ERROR", "密码长度错误"),
    PASSWORD_STRENGTH_LOW(20107, "PASSWORD_STRENGTH_LOW", "密码强度不够"),
    VERIFICATION_CODE_ERROR(20108, "VERIFICATION_CODE_ERROR", "验证码错误"),
    MESSAGE_VERIFICATION_CODE_ERROR(20109, "MESSAGE_VERIFICATION_CODE_ERROR", "短信验证码错误"),
    PHONE_VERIFICATION_CODE_ERROR(20110, "EMAIL_VERIFICATION_CODE_ERROR", "电话验证码错误"),
    LOGIN_EXCEPTION(20200, "LOGIN_EXCEPTION", "登录异常"),
    USER_ACCOUNT_NOT_EXISTS(20201, "USER_ACCOUNT_NOT_EXISTS", "用户账户不存在"),
    USER_PASSWORD_ERROR(20202, "USER_PASSWORD_ERROR", "用户密码错误"),
    USER_ACCOUNT_FROZEN(20203, "USER_ACCOUNT_FROZEN", "用户账户被冻结"),
    USER_UPDATE_EROOR(20204, "USER_UPDATE_EROOR", "用户信息更新失败"),
    TOKEN_NULL_ERROR(20205, "TOKEN_ERROR", "TOKEN信息缺少"),
    TOKEN_ERROR(20205, "TOKEN_ERROR", "TOKEN信息错误"),

    /**
     * <p>NOTICE_ERROR</p>
     */
    NOTICE_ERROR(20301,"NOTICE_ERROR","公告或新闻信息不全"),
    NOTICE_NOEXIST(20302,"NOTICE_NOEXIST","公告或新闻信息不存在"),
    NOTICE_COMMENTS_ERROR(20303,"NOTICE_COMMENTS_ERROR","公告或新闻评论信息不全"),
    NOTICE_COMMENTSID_ERROR(20304,"NOTICE_COMMENTSID_ERROR","公告或新闻评论ID不存在"),

    /**
     * <p>MOMENTS_ERROR</p>
     */
    MOMENTS_ERROR(20401,"MOMENTS_ERROR","动态信息不全"),
    MOMENTS_LIKES_ERROR(20402,"MOMENTS_LIKES_ERROR","已经点赞了"),

    /**
     * <p>COMMENTS_ERROR</p>
     */
    COMMENTS_ERROR(20501,"COMMENTS_ERROR","会话信息不全"),
    CONNECT_ERROR(20502,"CONNECT_ERROR_ERROR","好友没有上线"),

    /**
     * <p>SERVER_ERROR</p>
     */
    SERVER_ERROR(30000, "SERVER_ERROR", "服务器错误");

    private final int code;
    private final String message;
    private final String description;

}
