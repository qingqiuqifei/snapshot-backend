package com.example.snapshot_system.mq;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

@Configuration
public class SimpleRabbitConfig {

    @Bean
    public Queue hello() {
        return new LinkedList(Collections.singleton("simple.hello"));
    }

    @Bean
    public SimpleSender simpleSender(){
        return new SimpleSender();
    }

    @Bean
    public SimpleReceiver simpleReceiver(){
        return new SimpleReceiver();
    }

}
