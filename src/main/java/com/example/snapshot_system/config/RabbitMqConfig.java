package com.example.snapshot_system.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class RabbitMqConfig implements InitializingBean{
    public static final String DIRECT_EXCHANGE = "eujian.exchange.direct";
    public static final String DIRECT_QUEUE = "eujian.queue.direct";
    @Autowired
    private AmqpAdmin amqpAdmin;
    @Override
    public void afterPropertiesSet() throws IOException {
        DirectExchange directExchange = new DirectExchange(DIRECT_EXCHANGE);
        Queue queue = new Queue(DIRECT_QUEUE);
        Binding binding = BindingBuilder.bind(queue).to(directExchange).withQueueName();
        amqpAdmin.declareExchange(directExchange);
        amqpAdmin.declareQueue(queue);
        amqpAdmin.declareBinding(binding);
    }
}
