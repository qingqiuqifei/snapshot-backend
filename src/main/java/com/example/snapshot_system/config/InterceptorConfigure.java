package com.example.snapshot_system.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.example.snapshot_system.interceptor.JWTInterceptor;

/**
 * <p> FileName: InterceptorConfig <p>
 * <p> Description: 拦截器配置 <p>
 * <p> Created By IntelliJ IDEA </p>
 * @author JIN
 * @since 2021/7/19
 *
 * @apiNote 可以静态调用
 */

@Configuration
public class InterceptorConfigure implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        /* 添加JWT拦截器 */
//        registry.addInterceptor(new JWTInterceptor())
//                .addPathPatterns("/**/**")
//                /* 不拦截登录接口 */
//                .excludePathPatterns("/sys-user/login")
//                /* 不拦截注册 */
//                .excludePathPatterns("/sys-user/register")
//                /* 不拦截SWAGGER */
//                .excludePathPatterns("/doc.html");
    }

}
