package com.example.snapshot_system.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.snapshot_system.common.HttpStatus;
import com.example.snapshot_system.common.ServerResponse;

import java.util.Calendar;
import java.util.Map;

/**
 * <p> FileName: JWTUtils <p>
 * <p> Description: JWT工具类 <p>
 * <p> Created By IntelliJ IDEA </p>
 *
 * @author JIN
 * @since 2021/7/19
 *
 * @apiNote 可以静态调用
 */

public class JWTUtils {

    /**
     * <p>签名的密钥[@Name]</p>
     */
    private static final String SECRET = "@QKY";

    /**
     * <p>签名的过期时间[A Week]</p>
     */
    private static final int ACCESS_TOKEN_EXPIRE_TIME = 604800;

    /**
     * <p>令牌颁布者身份标识[Domain]</p>
     */
    private static final String ISSUER = "com.example";

    /**
     * <p>生成token</p>
     * @param chaims
     * @return
     */
    public static String generateToken(Map<String, String> chaims) {
        JWTCreator.Builder builder = JWT.create();
        /* 添加负载 */
        chaims.forEach((k, v) -> { builder.withClaim(k, v); });
        /* 设置过期时间 */
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, ACCESS_TOKEN_EXPIRE_TIME);
        builder.withExpiresAt(calendar.getTime());
        /* 设置颁发者身份 */
        builder.withIssuer(ISSUER);
        /* 设置加密算法以及密钥 */
        String token = builder.sign(Algorithm.HMAC256(SECRET));
        return token;
    }

    /**
     * <p>解析token</p>
     * @param token
     * @return
     */
    public static DecodedJWT verifyToken(String token) {
        return JWT.require(Algorithm.HMAC256(SECRET)).build().verify(token);
    }

    /**
     * <p>通过ID、phone生成Token</p>
     * @param
     * @return
     */
    public static String generateTokenById(Long id,String phone) {
        JWTCreator.Builder builder = JWT.create();
        /* 添加负载 */
        builder.withClaim("id", id);
        builder.withClaim("phone", phone);
        /* 设置过期时间 */
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, ACCESS_TOKEN_EXPIRE_TIME);
        builder.withExpiresAt(calendar.getTime());
        /* 设置颁发者身份 */
        builder.withIssuer(ISSUER);
        /* 设置加密算法以及密钥 */
        String token = builder.sign(Algorithm.HMAC256(SECRET));
        return token;
    }

    /**
     * <p>解析token中的内容</p>
     * @param token
     * @return
     */
    public static String getTokenPhone(String token){
        DecodedJWT decodedJWT = JWTUtils.verifyToken(token);
        Long id = decodedJWT.getClaim("id").asLong();
        String phone = decodedJWT.getClaim("phone").asString();
        return phone;
    }

    /**
     * <p>验证token中的电话号码</p>
     * @param token
     * @return
     */
//    String token = CheckUtils.checkToken(request.getHeader("Authorization"));
//        if(token.equals(null)){
//        return  ServerResponse.dataResponse(HttpStatus.TOKEN_NULL_ERROR);
//    }
//        if(!phone.equals(JWTUtils.getTokenPhone(token))){
//        return ServerResponse.dataResponse(HttpStatus.TOKEN_ERROR);
//    }
}
