package com.example.snapshot_system.util;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 阿里云OSS配置
 * @author jin
 */
@Component
public class ConstantPropertiesUtils implements InitializingBean {

    /**
     * 读取配置内容
     */
    @Value("${aliyun.oss.file.endpoint}")
    private String endpoint;

    @Value("${aliyun.oss.file.keyid}")
    private String keyid;

    @Value("${aliyun.oss.file.keysecret}")
    private String keysecret;

    @Value("${aliyun.oss.file.bucketname}")
    private String bucketname;

    /**
     * 定义公开的静态常量
     */
    public static String END_POINT;
    /**
     * 定义公开的静态常量
     */
    public static String ACCESS_KEY_ID;
    /**
     * 定义公开的静态常量
     */
    public static String ACCESS_KEY_SECRET;
    /**
     * 定义公开的静态常量
     */
    public static String BUCKET_NAME;

    @Override
    public void afterPropertiesSet() throws Exception {
        END_POINT = endpoint;
        ACCESS_KEY_ID = keyid;
        ACCESS_KEY_SECRET = keysecret;
        BUCKET_NAME = bucketname;

    }
}
