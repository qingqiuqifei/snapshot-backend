package com.example.snapshot_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.snapshot_system.vo.CommentVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 会话信息整合评论 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */

public interface CommentVOMapper extends BaseMapper<CommentVO> {
    //1v1时返回头像和姓名
    //SELECT c.*,s.name as phone_send_name,s.avatar as phone_send_avatar,s1.name as phone_received_name,s1.avatar as phone_received_avatar from comments as c,sys_user as s,sys_user as s1 where c.phone_send=s.phone and c.phone_received=s1.phone and c.phone_send=18755617490 and c.phone_received=19855109876 ORDER BY c.create_time;
    @Select("SELECT c.*,s.name as phone_send_name,s.avatar as phone_send_avatar,s1.name as phone_received_name,s1.avatar as phone_received_avatar from comments as c,sys_user as s,sys_user as s1 where c.phone_send=s.phone and c.phone_received=s1.phone and c.phone_send=#{phone} and c.isNewest=1 ORDER BY c.create_time;")
    List<CommentVO> getAllComentVo(@Param("phone") String phone);
}
