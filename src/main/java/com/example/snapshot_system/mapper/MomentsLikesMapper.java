package com.example.snapshot_system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.snapshot_system.entity.MomentsLikes;

/**
 * <p>
 * 朋友圈的点赞 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface MomentsLikesMapper extends BaseMapper<MomentsLikes> {

}
