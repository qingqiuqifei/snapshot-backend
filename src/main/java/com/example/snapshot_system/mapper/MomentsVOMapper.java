package com.example.snapshot_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.snapshot_system.entity.Moments;
import com.example.snapshot_system.vo.MomentsVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 朋友圈整合评论 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface MomentsVOMapper extends BaseMapper<MomentsVO> {
//    @Select("SELECT u.*, (SELECT COUNT(id) FROM moments_comments AS r WHERE r.id = u.id) AS commentsCounts FROM moments AS u LEFT JOIN moments_comments AS r ON u.id = r.id GROUP BY u.id ORDER BY u.gmt_time DESC;")
    @Select("SELECT u.*, (SELECT COUNT(id) FROM moments_comments AS r WHERE r.id = u.id) AS commentsCounts,(SELECT count(*) FROM moments_likes as m WHERE phone_called=#{phone} and m.id=u.id) AS isLiked FROM moments AS u LEFT JOIN moments_comments AS r ON u.id = r.id GROUP BY u.id ORDER BY u.gmt_time DESC;")
    List<MomentsVO> getMomentsVo(@Param("phone") String phone);


}
