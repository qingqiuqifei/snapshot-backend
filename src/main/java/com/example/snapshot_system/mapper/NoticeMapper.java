package com.example.snapshot_system.mapper;

import com.example.snapshot_system.entity.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公告 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
