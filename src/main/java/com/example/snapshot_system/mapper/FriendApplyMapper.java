package com.example.snapshot_system.mapper;

import com.example.snapshot_system.entity.FriendApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 申请好友的列表 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface FriendApplyMapper extends BaseMapper<FriendApply> {

}
