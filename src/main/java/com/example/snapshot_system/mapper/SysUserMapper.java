package com.example.snapshot_system.mapper;

import com.example.snapshot_system.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-16
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

}
