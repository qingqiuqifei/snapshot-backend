package com.example.snapshot_system.mapper;

import com.example.snapshot_system.entity.NoticeComments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 新闻的回复 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface NoticeCommentsMapper extends BaseMapper<NoticeComments> {

}
