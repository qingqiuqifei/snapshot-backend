package com.example.snapshot_system.mapper;

import com.example.snapshot_system.entity.Moments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 朋友圈 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface MomentsMapper extends BaseMapper<Moments> {
}
