package com.example.snapshot_system.mapper;

import com.example.snapshot_system.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会话信息表 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface CommentMapper extends BaseMapper<Comment> {

}
