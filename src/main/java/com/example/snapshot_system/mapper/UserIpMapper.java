package com.example.snapshot_system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.snapshot_system.entity.UserIp;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户号码对应ip Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface UserIpMapper extends BaseMapper<UserIp> {

}
