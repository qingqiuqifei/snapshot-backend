package com.example.snapshot_system.mapper;

import com.example.snapshot_system.entity.FriendOwn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 好友列表 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface FriendOwnMapper extends BaseMapper<FriendOwn> {

}
