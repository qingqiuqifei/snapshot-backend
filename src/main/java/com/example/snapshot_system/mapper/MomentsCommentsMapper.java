package com.example.snapshot_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.snapshot_system.entity.MomentsComments;

/**
 * <p>
 * 朋友圈的评论 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface MomentsCommentsMapper extends BaseMapper<MomentsComments> {

}
