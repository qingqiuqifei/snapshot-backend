package com.example.snapshot_system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.snapshot_system.vo.FriendOwnVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 好友列表整合姓名，头像 Mapper 接口
 * </p>
 *
 * @author jin
 * @since 2021-07-19
 */
public interface FriendOwnVoMapper extends BaseMapper<FriendOwnVo> {

}
